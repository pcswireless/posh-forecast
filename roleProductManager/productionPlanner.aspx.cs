﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PoshForecastingTool.roleProductManager
{
    public partial class productionPlanner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HyperLink btn;
            btn = (HyperLink)Master.Master.FindControl("lnkProduction");
            btn.CssClass += " selected";
            btn.NavigateUrl = "~/roleProductManager/productionPlanner.aspx";
        }
    }
}