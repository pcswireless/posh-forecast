﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PoshForecastingTool
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string role = "manager";

            switch (role)
            {
                case "manager":
                    Server.Transfer("~/roleManager/default.aspx");
                    break;
                default:
                    Server.Transfer("~/roleManager/default.aspx");
                    break;
            }

        }
    }
}