﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PoshForecastingTool.roleManager
{
    public partial class forecastSales : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HyperLink btn = (HyperLink) Master.Master.FindControl("lnkSales");
            btn.CssClass += " selected";

            btn = (HyperLink)Master.Master.FindControl("lnkEdit");
            btn.NavigateUrl = "~/roleManager/forecastSalesEdit.aspx";
        }
    }
}