﻿$(document).on("mouseover", "tr", function () {
    $(this).addClass('selected');
});
$(document).on("mouseout", "tr", function () {
    $(this).removeClass('selected');
});

$(document).on("click", ".tblForecast td.clickable", function () {
    var that = $(this);
    var col = that.index();
    var row = that.closest('tr').index();
    var columnHeader = that.closest('table').children('thead').children('tr:nth-of-type(2)').children('th:nth-of-type(' + col + ')').text();
    var itemName = that.closest('table').children('tbody').children('tr:nth-of-type(' + (row + 1) + ')').children('td:nth-of-type(1)').text();

    $('.infoBlock .header').text(itemName+ ' / ' + columnHeader);

    $('.tblForecast td.clickable').removeClass('selected');
    that.addClass('selected');

    //debugger;
});



/*
 *
 * Prepare table data
 * 
 */

var dataFromServer;
var mainTableData = [];

$.holdReady(true);
var tableDataGet = $.get("/ajax/productionPlanData.ashx");
tableDataGet.always(function (data) {
    dataFromServer = data;

    // create data for table display
    $(data.rows).each(function () {
        var rowData = this.cellValues;
        mainTableData.push(rowData);
    });
    $.holdReady(false);
});


$(document).ready(function () {
    tableCreate.create(productPlannerTableConfig, mainTableData);

    // add clickable class for cells
    $('td').each(function () {

        var that = $(this);
        var col = that.index();
        if (col >= 4) {
            $(this).addClass('clickable');
        };
    });

});