
/*
 *
 * Handle popover events
 *
 */

$(document).on("click", ".tblForecast td.clickable", function () {
    var that = $(this);
    var col = that.index();
    var row = that.closest('tr').index();

    $('[data-original-title]').popover('destroy');


    var data = findHistory(row, col);
    var popoverContent ="<table class=\"historyPopover\">" + buildTableFromData(data) + "</table>";

    that.popover(
        {
            title: "Previous values",
            html: true,
            content: popoverContent,
            placement: "top",
            container: ".popoverPlaceholder"
        }
    );
    that.popover('show');
});

// destroy popover on click outside
$(document).on('click', function (e) {
    if (typeof $(e.target).data('original-title') == 'undefined') {
        $('[data-original-title]').popover('destroy');
    }
});

// destroy popover on escape
$(document).keyup(function (event) {
    if (event.which === 27) {
        $('[data-original-title]').popover('destroy');
    }
});


var findHistory = function (row, col) {
    return dataFromServer.history.filter(
        function (data) {
            return (data.row==row && data.col==col);
        }
    )[0];
}


var buildTableFromData = function (data) {
    var tableData = [];

    $.each(data.cellHistory, function (index, value) {
        tableData.push([value.week, value.value]);
    });

    var tableConfig = {
        "id": "",
        "defs": {
            "headers": [
                [
                    {
                        "title": "Week"
                    },
                    {
                        "title": "Value"
                    }
                ]
            ]
        }
    };
    var tableHTML = tableCreate.create(tableConfig, tableData);
    return tableHTML;

}



/*
 *
 * Prepare table data
 * 
 */

var dataFromServer;
var mainTableData = [];

$.holdReady(true);
var tableDataGet = $.get("/ajax/salesForecastHistory.ashx");

tableDataGet.always(function (data) {

    dataFromServer = data;

    // create data for table display
    $(data.rows).each(function () {
        var rowData = this.cellValues;
        mainTableData.push(rowData);
    });
    $.holdReady(false);
});


$(document).ready(function () {
    var expandable = true;
    tableCreate.create(forecastTableConfig, mainTableData);

    // add clickable class for cells with historyData
    $('td').each(function () {


        var minCol = 8; // set minimum column for sales forcast update

        var that = $(this);
        var col = that.index();
        var row = that.closest('tr').index();
        var cellHistory = findHistory(row, col);

        if (cellHistory && (col >= minCol) && (cellHistory.cellHistory.length > 0)) {
            that.addClass("clickable").append('<div class="popoverPlaceholder">');

            // display "significant change" indicator
            var currentValue = parseInt($(this).text());
            var previousValue = parseInt(cellHistory.cellHistory[cellHistory.cellHistory.length-1].value);
            var change = currentValue - previousValue;
            var significantChange = previousValue / 5;

            if  ((change) && (Math.abs(change) > significantChange)) {
                if (change < 0) {
                    that.addClass("downArrow");
                } else {
                    that.addClass("upArrow");
                }
            }
        }
    });




    // make table expandable
    //var expandable = true;
    //var table = $('#tablePlaceholder');

    //expandabeTable = tableExpandable.setTable(table);



    var et = newExpandableTable();
    et.expandable = true;
    var table = $('#tablePlaceholder');
    et.setTable(table);
    
});