$.holdReady(true);
var historyData;
$.getScript("../js/sample_historyData.js", function () {
    
    $.holdReady(false);
});


/*
 *
 * Handle popover events
 *
 */

$(document).on("click", ".tblForecast td.clickable", function () {

    var that = $(this);
    var col = that.index();
    var row = that.closest('tr').index();

    $('[data-original-title]').popover('destroy');

    var data = findData(row, col);
    var popoverContent = buildTableFromData(data);



    that.popover(
        {
            title: "Previous values",
            html: true,
            content: popoverContent,
            placement: "top",
            container: ".popoverPlaceholder"
        }
    );
    that.popover('show');
});

// destroy popover on click outside
$(document).on('click', function (e) {
    if (typeof $(e.target).data('original-title') == 'undefined') {
        $('[data-original-title]').popover('destroy');
    }
});

// destroy popover on escape
$(document).keyup(function (event) {
    if (event.which === 27) {
        $('[data-original-title]').popover('destroy');
    }
});


var findData = function (row, col) {
    return historyData.filter(
        function (data) {
            return data.row == row && data.col == col
        }
    );
}


var buildTableFromData = function (data) {
    var tableData = [];
    $.each(data[0].weeks, function (index, value) {
        tableData.push([value.w, value.v]);
    });

    var tableConfig = {
        "id": "",
        "defs": {
            "headers": [
                [
                    {
                        "title": "Week"
                    },
                    {
                        "title": "Value"
                    }
                ]
            ]
        }
    };
    var tableHTML = tableCreate.create(tableConfig, tableData);
    return tableHTML;

}



/*
 *
 * Document ready
 * 
 */
$(document).ready(function () {

    var dataLocation = forecastTableConfig.data;

    $.get(dataLocation, function (data) {
        tableCreate.create(forecastTableConfig, data);

        // add clickable class for cells with historyData
        $('td').each(function () {
            var that = $(this);
            var col = that.index();
            var row = that.closest('tr').index();
            var cellData = findData(row, col);

            if (cellData.length > 0) {
                that.addClass("clickable").append('<div class="popoverPlaceholder">');

                if ((col + row) % 2 == 0) {
                    that.addClass("downArrow");
                } else {
                    that.addClass("upArrow");
                }
            }
        });
    });

});