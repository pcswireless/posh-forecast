﻿var productPlannerTableConfig = {
    "id": "productionPlan",
    "tableClass": "tblProductionPlan",
    "defs": {
        "headers": [
            [
                {
                    "title": "Item",
                    "colspan": 1,
                    "rowspan": 2
                },
                {
                    "title": "History",
                    "colspan": 3
                },
                {
                    "title": "Committed",
                    "colspan": 4
                },
                {
                    "title": "Forecast"
                }
            ],
            [
                {
                    "title": "W-3"
                },
                {
                    "title": "W-2"
                },
                {
                    "title": "W-1"
                },
                {
                    "title": "W1"
                },
                {
                    "title": "W2"
                },
                {
                    "title": "W3"
                },
                {
                    "title": "W4"
                },
                {
                    "title": "W5"
                }
            ]
        ]
    }
};