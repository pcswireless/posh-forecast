﻿<%@ Page Title="" Language="C#" MasterPageFile="~/roleManager/managerMaster.master" AutoEventWireup="true" CodeBehind="forecastSales.aspx.cs" Inherits="PoshForecastingTool.roleManager.forecastSales" %>

<asp:Content ID="pageScripts" ContentPlaceHolderID="scriptPlaceHolder" runat="server">
    <script type="text/javascript" src="/js/forecastTableConfig.js"></script>
    <script type="text/javascript" src="/js/products.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {


            //$('.selected').removeClass('selected');
            //$('#lnkSales').addClass('selected');
        });
    </script>
</asp:Content>

<asp:Content ID="headerContent" ContentPlaceHolderID="submasterHeadPlaceHolder" runat="server">
    <script type="text/javascript" src="/js/tableCreate.js"></script>
    <link rel="stylesheet" href="/css/productsTable.css">
</asp:Content>

<asp:Content ID="pageContent" ContentPlaceHolderID="masterContentPlaceHolder" runat="server">
    <div class="row">
        <table id="tablePlaceholder" class="col-lg-12"></table>
    </div>
</asp:Content>