﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Script.Serialization;

namespace PoshForecastingTool.ajax
{
    public class salesForecastHistory : IHttpHandler
    {

        public class CellHistory
        {
            public int week { get; set; }      
            public string value { get; set; }   
        }

        public class CellHistoryRecord
        {
            public int row { get; set; }
            public int col { get; set; }
            public List<CellHistory> cellHistory { get; set; }
        }

        public class RowData
        {
            public List<string> cellValues { get; set; }
        }

        public class TableData
        {
            public List<RowData> rows { get; set; }
            public List<CellHistoryRecord> history { get; set; }
        }


        private TableData buildDummyData()
        {
            TableData tableData = new TableData();
            tableData.rows = new List<RowData>();
            tableData.history = new List<CellHistoryRecord>();

            RowData rowData;
            CellHistory cellHistory;
            CellHistoryRecord cellHistoryRecord;      

            Random rnd = new Random();
            for (int row = 0; row < 6; row++)
            {
                
                // build table row data
                rowData = new RowData();
                rowData.cellValues = new List<string>();     
                
                for (int col = 0; col <= 18; col++)
                {
                    // build cell (row,col) data
                    string cellValue = row.ToString() + col.ToString() +rnd.Next(0,99);
                    if (col == 0)
                    {
                        cellValue = "Item " + row;
                    }
                    rowData.cellValues.Add(cellValue);

                    // build historical data for (week, row, col)
                    if (col >= 4 && col < 18)
                    {
                        cellHistoryRecord = new CellHistoryRecord();
                        cellHistoryRecord.cellHistory = new List<CellHistory>();
                        cellHistoryRecord.row = row;
                        cellHistoryRecord.col = col;

                        int maxWeek = rnd.Next(0, 10);
                        for (int week = maxWeek; week >= 1; week--)
                        {
                            cellHistory = new CellHistory();
                            cellHistory.week = -week;
                            cellHistory.value = rnd.Next(100, 10000).ToString();

                            cellHistoryRecord.cellHistory.Add(cellHistory);
                        }
                        tableData.history.Add(cellHistoryRecord);
                    }
                }

                tableData.rows.Add(rowData);

            }
            return tableData;
        }


        public void ProcessRequest(HttpContext context)
        {
            TableData data = buildDummyData();
            
            context.Response.ContentType = "application/json";
            var json = new JavaScriptSerializer();
            string jsonString = json.Serialize(data);

            context.Response.Write(jsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}